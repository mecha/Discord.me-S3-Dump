var stickyPromoAnchor = $('.sticky-promo-anchor').offset();

$(window).scroll(function() {
    xPos = $(this).scrollTop();

    stickyPromo(xPos, stickyPromoAnchor.top);
});

function stickyPromo(xPos, anchorPos)
{
    var offsetBy = 90;
    var offsetPos = anchorPos - offsetBy;

    if (xPos > offsetPos) {
        $('.sticky-promo').css('position', 'fixed');
        $('.sticky-promo').css('top', offsetBy);
    } else {
        $('.sticky-promo').css('position', 'relative');
        $('.sticky-promo').css('top', 0);
    }
}