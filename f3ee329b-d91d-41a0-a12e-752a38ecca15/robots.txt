User-agent: *
Disallow: /server/join/
Disallow: /server/article/paginate/
Disallow: /cr/

Sitemap: https://discord.me/sitemap
